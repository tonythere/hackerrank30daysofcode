package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindMax(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name     string
		input    string
		expected int
	}{
		{"sample 1", "5", 1},
		{"sample 2", "13", 2},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			got := resolve(tc.input)
			assert.Equal(t, tc.expected, got)
		})
	}
}
